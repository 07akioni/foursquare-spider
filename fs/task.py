from fs.config import AreaConfig

def loop_area(area_config: AreaConfig, task, interval_time):
    start_lat = area_config.start_lat
    end_lat   = area_config.end_lat
    start_lon = area_config.start_lon
    end_lon   = area_config.end_lon
    lat_interval = area_config.lat_interval
    lon_interval = area_config.lon_interval
    lat_tick = (end_lat - start_lat) / lat_interval
    lon_tick = (end_lon - start_lon) / lon_interval
    for i in range(0, lat_interval + 1):
        for j in range(0, lon_interval + 1):
            lat = start_lat + i * lat_tick
            lon = start_lon + j * lon_tick
            print(lat, lon)
    pass

def download_area_venues(area_config: AreaConfig):
    pass

def download_venue_likes():
    pass

def download_profile_from_likes():
    pass
        