import requests
import json
import time
from bs4 import BeautifulSoup

class FourSquareApi:
    def __init__(self, client_id, client_secret, **kargs):
        self.client_id = client_id
        self.client_secret = client_secret
        if kargs['access_token']:
            self.access_token = kargs['access_token']
    def _v(self):
        return time.strftime('%Y%m%d')
    def oauth_login(self, redirect_uri, username, password):
        print('https://foursquare.com/oauth2/authenticate?client_id=%s&response_type=code&redirect_uri=%s' % (self.client_id, redirect_uri))
        print('打开这个地址，然后登陆，然后把CODE输入，之后回车')
        code = input()
        print('https://foursquare.com/oauth2/access_token?client_id=%s&client_secret=%s&redirect_uri=%s&code=%s' % (self.client_id, self.client_secret, redirect_uri, code))
        response = requests.get('https://foursquare.com/oauth2/access_token?client_id=%s&client_secret=%s&grant_type=authorization_code&redirect_uri=%s&code=%s' % (self.client_id, self.client_secret, redirect_uri, code))
        self.access_token = json.loads(response.text)['access_token']
        print(self.access_token)
        print(response.text)
    def checkin_detail(self, checkin_id):
        response = requests.get('https://api.foursquare.com/v2/checkins/%s' % checkin_id, params={
            'v': self._v(),
            'oauth_token': self.access_token
        })
        print(response.text)
    def get_likes_of_venue(self, venue_id):
        response = requests.get('https://api.foursquare.com/v2/venues/%s/likes' % venue_id, params={
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'v': self._v()
        })
        return json.loads(response.text)
    def search_for_venues(self, lon, lat):
        response = requests.get('https://api.foursquare.com/v2/venues/search', params={
            'll': '%lf,%lf' % (lon, lat),
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'v': self._v()
        })
        return json.loads(response.text)
    def get_profile(self, user_id):
        response = requests.get('https://api.foursquare.com/v2/users/%s' % user_id, params={
            'oauth_token': self.access_token,
            'v': self._v()
        })
        return json.loads(response.text)

if __name__ == '__main__':
    pass