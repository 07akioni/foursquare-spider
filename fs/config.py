class AreaConfig:
    def __init__(self, start_lat, end_lat, start_lon, end_lon, lat_interval, lon_interval):
        self.start_lat = start_lat
        self.end_lat   = end_lat
        self.start_lon = start_lon
        self.end_lon   = end_lon
        self.lat_interval = lat_interval
        self.lon_interval = lon_interval