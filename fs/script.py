from fs.config import AreaConfig
from fs.api import FourSquareApi
from os import path
import os
import json
import traceback
import time

def loop_area(area_config: AreaConfig, task, interval_time):
    start_lat = area_config.start_lat
    end_lat   = area_config.end_lat
    start_lon = area_config.start_lon
    end_lon   = area_config.end_lon
    lat_interval = area_config.lat_interval
    lon_interval = area_config.lon_interval
    lat_tick = (end_lat - start_lat) / lat_interval
    lon_tick = (end_lon - start_lon) / lon_interval
    for i in range(0, lat_interval + 1):
        for j in range(0, lon_interval + 1):
            lat = start_lat + i * lat_tick
            lon = start_lon + j * lon_tick
            task(lon, lat)
            time.sleep(interval_time)
            # print(lat, lon)
    pass

def download_area_venues(area_config: AreaConfig):
    pass

def download_like_from_venues(fs_api: FourSquareApi):
    fileNames = os.listdir('data/search')
    count = 0
    for fileName in fileNames:
        searchResponseFile = open('data/search/' + fileName, 'r')
        searchResponse = searchResponseFile.read()
        searchResponseFile.close()
        try:
            venues = json.loads(searchResponse)['response']['venues']
            count += len(venues)
        except:
            pass
    print(count)
    cnt = 0
    for fileName in fileNames:
        searchResponseFile = open('data/search/' + fileName, 'r')
        searchResponse = searchResponseFile.read()
        searchResponseFile.close()
        try:
            venues = json.loads(searchResponse)['response']['venues']
        except:
            traceback.print_exc()
            continue
        try:
            for venue in venues:
                cnt += 1
                print('%d / %d' % (cnt, count))
                venue_id = venue['id']
                print(venue_id)
                if path.exists('data/like/%s.json' % venue_id):
                    print('exists')
                    continue
                else:
                    try:
                        response = fs_api.get_likes_of_venue(venue_id)
                        likeFile = open('data/like/%s.json' % venue_id, 'w')
                        likeFile.write(json.dumps(response))
                        likeFile.close()
                        time.sleep(1)
                    except:
                        traceback.print_exc()
                        continue
        except:
            traceback.print_exc()
            continue
    pass

def download_profile_from_likes(fs_api):
    fileNames = os.listdir('data/like')
    count = 0
    user_id_set = set()
    for fileName in fileNames:
        likeResponseFile = open('data/like/' + fileName, 'r')
        likeResponse = likeResponseFile.read()
        likeResponseFile.close()
        try:
            likes = json.loads(likeResponse)['response']['likes']['items']
            # print(likes)
            for like in likes:

                user_id_set.add(like['id'])
        except:
            # traceback.print_exc()
            pass
    print(len(user_id_set))
    cnt = 0
    for user_id in user_id_set:
        if path.exists('data/profile/%s.json' % user_id):
            print('exists')
            continue
        else:
            try:
                print(user_id)
                response = fs_api.get_profile(user_id)
                profileFile = open('data/profile/%s.json' % user_id, 'w')
                profileFile.write(json.dumps(response))
                profileFile.close()
                time.sleep(120)
                pass
            except:
                traceback.print_exc()
                continue
    return

def download_new_york_venues(fs_api: FourSquareApi):
    # newyork_config = {
    #     'start_lat': -74.25,
    #     'end_lat': -73.5,
    #     'start_lon': 40.5,
    #     'end_lon': 41.0,
    #     'lat_interval': 70,
    #     'lon_interval': 50,
    # }
    # Parameters after correction: 'start_lat': -74.25, 'end_lat': -73.68, 'start_lon': 40.5, 'end_lon': 41.0,
    def download_venues(lon, lat):
        lon = round(lon, 6)
        lat = round(lat, 6)
        try:
            print(lon, lat)
            venues = fs_api.search_for_venues(lon, lat)
            # print(venues)
            file_name = 'data/search/%lf,%lf.json' % (lon, lat)
            if not path.exists(file_name):
                file = open(file_name, 'w')
                file.write(json.dumps(venues))
                file.close()
            else:
                print(file_name, 'already exists')
        except:
            traceback.print_exc()
            return

    area_config = AreaConfig(-74.25, -73.68, 40.5, 41.0, 70, 50)
    loop_area(area_config, download_venues, 120)